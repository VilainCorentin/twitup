# TwitUp

TwitUp is a social networking system inspired by Twitter and developed in Java.

The particularity of the system is that it works only with a client (no server) and an allocated storage space (a directory on Linux/Windows).