package com.iup.tp.twitup.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;

import com.iup.tp.twitup.datamodel.Twit;
import com.iup.tp.twitup.datamodel.User;
import com.iup.tp.twitup.listener.ITwitListener;

public class TwitModel {

	protected Set<Twit> twits;
	
	private final Collection<ITwitListener> twitListeners = new ArrayList<ITwitListener>();
	
	public TwitModel(Set<Twit> twts) {
		this.twits =  twts;
	}
	
	public void addTwit(Twit t) {
		for(ITwitListener i : this.twitListeners) {
			i.getTwit(t);
		}
	}
	
    public void addUserListener(ITwitListener listener) {
        twitListeners.add(listener);
        
        for(Twit t : this.twits) {
        	listener.getTwit(t);
        }
        
    }
    
    public void setModel(Set<Twit> twit) {
    	this.twits = twit;
    	
    	for(Twit u : this.twits) {
    		this.addTwit(u);
    	}
    	 
    }
    
    public void refreshModel() {
    	for(Twit u : this.twits) {
    		this.addTwit(u);
    	}
    }
	
}