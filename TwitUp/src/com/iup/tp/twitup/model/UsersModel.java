package com.iup.tp.twitup.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;

import com.iup.tp.twitup.datamodel.User;
import com.iup.tp.twitup.listener.IUsersListener;

public class UsersModel {

	protected Set<User> users;
	
	private final Collection<IUsersListener> usersListeners = new ArrayList<>();

	public UsersModel(Set<User> users) {
		super();
		this.users = users;
	}
	
	public void addUserOther(User u) {
		for(IUsersListener i : this.usersListeners) {
			i.getOtherUser(u);
		}
	}
	
    public void addUserListener(IUsersListener listener) {
    	usersListeners.add(listener);
        
        for(User t : this.users) {
        	listener.getOtherUser(t);
        }
        
    }
    
    public void setModel(Set<User> users) {
    	this.users = users;
    	
    	for(User u : this.users) {
    		this.addUserOther(u);
    	}
    	 
    }
    
    public void refreshModel() {
    	for(User u : this.users) {
    		this.addUserOther(u);
    	}
    }
	
}
