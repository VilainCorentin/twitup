package com.iup.tp.twitup.model;

import java.util.ArrayList;
import java.util.Collection;

import com.iup.tp.twitup.core.EntityManager;
import com.iup.tp.twitup.datamodel.User;
import com.iup.tp.twitup.listener.IUserIsConnectedListener;

public class UserConnected {

	protected User user;
	protected Boolean isValid;
	
	protected EntityManager dataManager;
	
	private final Collection<IUserIsConnectedListener> userListeners = new ArrayList<IUserIsConnectedListener>();
	
	public UserConnected(EntityManager e) {
		this.user = null;
		this.dataManager = e;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User userTmp) {
		
		
		isValid = false;
		
		if(userTmp != null) {
			isValid = true;
		}
		
		this.user = userTmp;
		
		this.notifSubfollower();
		
	}
	
	public void addFollow(User u) {
		this.user.addFollowing(u.getUserTag());
		this.dataManager.sendUser(this.user);
		this.notifSubfollower();
	}
	
	public void removeFollow(User u) {
		this.user.removeFollowing(u.getUserTag());
		this.dataManager.sendUser(this.user);
		this.notifSubfollower();
	}
	
	public void changeUsername(String s) {
		this.user.setName(s);
		this.dataManager.sendUser(this.user);
		this.notifSubfollower();
	}
	
	public void changeAvatar(String s) {
		this.user.setAvatarPath(s);
		this.dataManager.sendUser(this.user);
	}
	
    public void addUserListener(IUserIsConnectedListener listener) {
        userListeners.add(listener);
    }
	
    
    private void notifSubfollower() {
		for(IUserIsConnectedListener u : this.userListeners) {
			u.userIsLog(isValid);
			u.getUser(this.user);
		}
    }
    
}
