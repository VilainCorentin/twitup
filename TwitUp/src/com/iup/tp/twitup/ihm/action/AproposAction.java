package com.iup.tp.twitup.ihm.action;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.JOptionPane;

import com.iup.tp.twitup.configue.Configurator;

public class AproposAction extends AbstractAction {

	private static final long serialVersionUID = 1838297793784254653L;
 
	public AproposAction( String texte){
		super(texte);
	}
 
	public void actionPerformed(ActionEvent e) { 
		JOptionPane.showMessageDialog(null, Configurator.getInstance().getTranslate("ACTION_PROPOS"));
	}
 
}