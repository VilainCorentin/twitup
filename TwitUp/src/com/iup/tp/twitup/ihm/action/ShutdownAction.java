package com.iup.tp.twitup.ihm.action;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import com.iup.tp.twitup.ihm.TwitupMainView;

public class ShutdownAction extends AbstractAction{

	private static final long serialVersionUID = -5912579615911856739L;
	
	public ShutdownAction(String texte){
		super(texte);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		System.exit(0);
	}
	
}