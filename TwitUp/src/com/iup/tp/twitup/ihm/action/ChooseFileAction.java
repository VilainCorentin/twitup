package com.iup.tp.twitup.ihm.action;

import java.awt.event.ActionEvent;
import java.io.File;
import java.util.ArrayList;

import javax.swing.AbstractAction;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileSystemView;

import com.iup.tp.twitup.configue.Configurator;
import com.iup.tp.twitup.controller.IUserRegister;
import com.iup.tp.twitup.core.Twitup;
import com.iup.tp.twitup.ihm.TwitupMainView;

public class ChooseFileAction extends AbstractAction{

	private static final long serialVersionUID = 6619153125916335303L;
	private JFileChooser jfc;
	private String path;

	public JFileChooser getJfc() {
		return jfc;
	}

	public void setJfc(JFileChooser jfc) {
		this.jfc = jfc;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public ChooseFileAction(String texte){
		super(texte);
		this.jfc = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
		this.jfc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		this.jfc.setAcceptAllFileFilterUsed(false);
	}
	
	public void actionPerformed(ActionEvent e) {
		
		int returnValue = jfc.showOpenDialog(null);
		
		if (returnValue == JFileChooser.APPROVE_OPTION) {
			File selectedFile = this.jfc.getSelectedFile();
			Configurator.getInstance().setProp("EXCHANGE_DIRECTORY_SAVE", selectedFile.getAbsolutePath());
		}
		
	}
	
}
