package com.iup.tp.twitup.ihm;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.iup.tp.twitup.configue.Configurator;
import com.iup.tp.twitup.controller.ILoginUser;
import com.iup.tp.twitup.controller.IUserRegister;
import com.iup.tp.twitup.datamodel.User;
import com.iup.tp.twitup.listener.IUserIsConnectedListener;
import com.iup.tp.twitup.view.LoginView;
import com.iup.tp.twitup.view.RegisterView;

public class LoginRegisterView extends JPanel implements IUserIsConnectedListener{

	private static final long serialVersionUID = -4150101466885553831L;
	
	private Configurator translate;
	
	private IUserRegister userRegister;
	private ILoginUser loginUser;
	
	public LoginRegisterView(IUserRegister iRegister, ILoginUser ILoginUser) {
		
		super(new GridBagLayout());
		
		this.translate = new Configurator();
		
		this.userRegister = iRegister;
		this.loginUser = ILoginUser;
		
		URL urlImg = getClass().getClassLoader().getResource(this.translate.getProp("IMG"));
		ImageIcon pImg = new ImageIcon(urlImg);
		JLabel lImg = new JLabel(pImg);
		
		JButton btnLogin = new JButton(this.translate.getTranslate("BTN_LOGIN"));
		btnLogin.addActionListener(new LoginView(this.loginUser));
		
		JButton btnRegister = new JButton(this.translate.getTranslate("BTN_REGISTER"));
		btnRegister.addActionListener(new RegisterView(this.userRegister));
		
		GridBagConstraints constraints = new GridBagConstraints();
	    constraints.insets = new Insets(10, 10, 10, 10);
		
	    constraints.gridx = 0;
	    constraints.gridy = 0;
	    constraints.gridwidth = 10;
	    constraints.anchor = GridBagConstraints.CENTER;
	    this.add(lImg, constraints);
	    
	    constraints.anchor = GridBagConstraints.WEST;
	    constraints.gridwidth = 1;
	    constraints.gridx = 0;
	    constraints.gridy = 1;     
	    this.add(btnLogin, constraints);
	    
	    constraints.gridx = 1; 
	    this.add(btnRegister, constraints);
	    
	}

	@Override
	public void userIsLog(Boolean bool) {
		this.setVisible(!bool);
	}

	@Override
	public void getUser(User user) {

	}
	
}
