package com.iup.tp.twitup.ihm;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import com.iup.tp.twitup.configue.Configurator;
import com.iup.tp.twitup.ihm.action.AproposAction;
import com.iup.tp.twitup.ihm.action.ChooseFileAction;
import com.iup.tp.twitup.ihm.action.ShutdownAction;

/**
 * Classe de la vue principale de l'application.
 */
public class TwitupMainView extends JFrame{
	
	private static final long serialVersionUID = 5672057643899920620L;
		
	public JMenuBar menuBar;
	
	public JMenu fileMenu;
	public JMenu otherMenu;
	
	public JMenuItem aPropos;
	public JMenuItem fileChooser;
	public JMenuItem shutButtom;
	
	public JPanel userPanel;
	public JPanel userSearch;
	public JPanel twitSearch;
	
	public JScrollPane twitsPanel;
	public JScrollPane usersPanel;
	
	public JPanel loginRegisterPanel;
	public JPanel addTwitPanel;
	
	private Configurator translate;

	//Constructeur de la main view
	public TwitupMainView() {
		super("Twittor");
		
		this.setLayout(new GridBagLayout());
		
		this.translate = new Configurator();
		
		this.menuBar = new JMenuBar();
		
		this.fileMenu = new JMenu(this.translate.getTranslate("MENU_FILE"));
		this.otherMenu = new JMenu("?");
		
		this.aPropos = new JMenuItem(new AproposAction(this.translate.getTranslate("MENU_PROPOS")));
		this.fileChooser = new JMenuItem(new ChooseFileAction(this.translate.getTranslate("MENU_OPEN_FILE")));
		this.shutButtom = new JMenuItem(new ShutdownAction(this.translate.getTranslate("MENU_EXIT")));
		
		this.shutButtom.setIcon(new ImageIcon(getClass().getResource("/images/exitIcon_20.png")));
		
		this.fileMenu.add(fileChooser);
		this.fileMenu.add(shutButtom);
		
		this.otherMenu.add(aPropos);
		
		this.menuBar.add(fileMenu);
		this.menuBar.add(otherMenu);
		
		this.setJMenuBar(this.menuBar);
		
		WindowListener l = new WindowAdapter() {
			public void windowClosing(WindowEvent e){
				System.exit(0);
			}
		};
		
		addWindowListener(l);
		
		Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
		this.setLocation((screenSize.width - this.getWidth()) / 6,
				(screenSize.height - this.getHeight()) / 4);
		
		setSize(screenSize.width / 3,screenSize.height / 3);
	}

	public JPanel getUserPanel() {
		return userPanel;
	}

	public void setUserPanel(JPanel userPanel) {
		this.userPanel = userPanel;
		this.add(this.userPanel, new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.FIRST_LINE_START, GridBagConstraints.HORIZONTAL, new Insets(1, 0, 0, 0), 0, 0));
	}

	public JScrollPane getTwitsPanel() {
		return twitsPanel;
	}

	public void setTwitsPanel(JScrollPane twitsPanel) {
		this.twitsPanel = twitsPanel;
		this.add(this.twitsPanel, new GridBagConstraints(0, 5, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
	}
	
	public void setUsersPanel(JScrollPane usersPanel) {
		this.usersPanel = usersPanel;
		this.add(this.usersPanel, new GridBagConstraints(0, 2, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
	}

	public JPanel getLoginRegisterPanel() {
		return loginRegisterPanel;
	}

	public void setLoginRegisterPanel(JPanel loginRegisterPanel) {
		this.loginRegisterPanel = loginRegisterPanel;
		this.add(this.loginRegisterPanel, new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
	}

	public JPanel getAddTwitPanel() {
		return addTwitPanel;
	}

	public void setAddTwitPanel(JPanel addTwitPanel) {
		this.addTwitPanel = addTwitPanel;
		this.add(this.addTwitPanel, new GridBagConstraints(0, 3, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(1, 0, 0, 0), 0, 0));
	}

	public void setUserSearch(JPanel userSearch) {
		this.userSearch = userSearch;
		this.add(this.userSearch, new GridBagConstraints(0, 1, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
	}

	public void setTwitSearch(JPanel twitSearch) {
		this.twitSearch = twitSearch;
		this.add(this.twitSearch, new GridBagConstraints(0, 4, 1, 1, 1, 0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
	}
	
	public void popupNotif(String msg) {
		JOptionPane.showMessageDialog(this, msg);
	}
	
}
