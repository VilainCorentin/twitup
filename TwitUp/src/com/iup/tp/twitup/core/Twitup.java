package com.iup.tp.twitup.core;

import java.io.File;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import com.iup.tp.twitup.configue.Configurator;
import com.iup.tp.twitup.controller.MainController;
import com.iup.tp.twitup.datamodel.Database;
import com.iup.tp.twitup.datamodel.IDatabase;
import com.iup.tp.twitup.events.file.IWatchableDirectory;
import com.iup.tp.twitup.events.file.WatchableDirectory;
import com.iup.tp.twitup.ihm.TwitupMainView;
import com.iup.tp.twitup.ihm.TwitupMock;
import com.iup.tp.twitup.monitoring.MonitoringSySo;

/**
 * Classe principale l'application.
 * 
 * @author S.Lucas
 */
public class Twitup{
	/**
	 * Base de données.
	 */
	protected IDatabase mDatabase;

	/**
	 * Gestionnaire des entités contenu de la base de données.
	 */
	protected EntityManager mEntityManager;

	/**
	 * Vue principale de l'application.
	 */
	protected TwitupMainView mMainView;

	/**
	 * Classe de surveillance de répertoire
	 */
	protected IWatchableDirectory mWatchableDirectory;

	/**
	 * Répertoire d'échange de l'application.
	 */
	protected String mExchangeDirectoryPath;

	/**
	 * Indique si le mode bouchoné est activé.
	 */
	protected boolean mIsMockEnabled;

	/**
	 * Nom de la classe de l'UI.
	 */
	protected String mUiClassName;
	
	protected Configurator config;
	
	protected MainController mainController;

	/**
	 * Constructeur.
	 */
	public Twitup() {
		
		this.config = new Configurator();
		
		this.mIsMockEnabled = Boolean.parseBoolean(this.config.getProp("MOCK_ENABLED"));
		
		// Init du look and feel de l'application
		this.initLookAndFeel();

		// Initialisation de la base de données
		this.initDatabase();

		if (this.mIsMockEnabled) {
			// Initialisation du bouchon de travail
			this.initMock();
		}

		// Initialisation du répertoire d'échange
		this.initDirectory();
		
		this.initController();
		
	}

	protected void initController() {
		
		this.mainController = new MainController(this.mDatabase, this.mEntityManager);
		
	}
	
	/**
	 * Initialisation du look and feel de l'application.
	 */
	protected void initLookAndFeel() {
		try {
			if(this.config.getProp("UI_CLASS_NAME") != null) {
				UIManager.setLookAndFeel(this.config.getProp("UI_CLASS_NAME"));
			}else {
				UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
			}
	    } 
	    catch (UnsupportedLookAndFeelException e) {
	    }
	    catch (ClassNotFoundException e) {
	    }
	    catch (InstantiationException e) {
	    }
	    catch (IllegalAccessException e) {
	    }
	}

	/**
	 * Initialisation du répertoire d'échange (depuis la conf ou depuis un file
	 * chooser). <br/>
	 * <b>Le chemin doit obligatoirement avoir été saisi et être valide avant de
	 * pouvoir utiliser l'application</b>
	 */
	protected void initDirectory() {
		
		File f = null;
		
		if(this.config.getProp("EXCHANGE_DIRECTORY_SAVE") != null) {
			f = new File(this.config.getProp("EXCHANGE_DIRECTORY_SAVE"));
		}
			
		if(this.isValideExchangeDirectory(f)) {
			this.initDirectory(this.config.getProp("EXCHANGE_DIRECTORY_SAVE"));
		}else {
			this.initDirectory(this.config.getProp("EXCHANGE_DIRECTORY"));
		}
		
	}

	/**
	 * Indique si le fichier donné est valide pour servire de répertoire
	 * d'échange
	 * 
	 * @param directory
	 *            , Répertoire à tester.
	 */
	protected boolean isValideExchangeDirectory(File directory) {
		// Valide si répertoire disponible en lecture et écriture
		return directory != null && directory.exists() && directory.isDirectory() && directory.canRead()
				&& directory.canWrite();
	}

	/**
	 * Initialisation du mode bouchoné de l'application
	 */
	protected void initMock() {
		TwitupMock mock = new TwitupMock(this.mDatabase, this.mEntityManager);
		mock.showGUI();
	}

	/**
	 * Initialisation de la base de données
	 */
	protected void initDatabase() {
		mDatabase = new Database();
		this.mDatabase.addObserver(new MonitoringSySo());
		mEntityManager = new EntityManager(mDatabase);
	}

	/**
	 * Initialisation du répertoire d'échange.
	 * 
	 * @param directoryPath
	 */
	public void initDirectory(String directoryPath) {
		mExchangeDirectoryPath = directoryPath;
		mWatchableDirectory = new WatchableDirectory(directoryPath);
		mEntityManager.setExchangeDirectory(directoryPath);

		mWatchableDirectory.initWatching();
		mWatchableDirectory.addObserver(mEntityManager);
	}
	
}
