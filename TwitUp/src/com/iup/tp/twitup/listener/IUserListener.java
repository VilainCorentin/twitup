package com.iup.tp.twitup.listener;

import com.iup.tp.twitup.datamodel.User;

public interface IUserListener {

	public void logUser(User user);
	
}
