package com.iup.tp.twitup.listener;

import com.iup.tp.twitup.datamodel.User;

public interface IUserIsConnectedListener {

	public void userIsLog(Boolean bool);
	
	public void getUser(User user);
	
}
