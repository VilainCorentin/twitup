package com.iup.tp.twitup.listener;

import com.iup.tp.twitup.datamodel.Twit;

public interface ITwitListener {

	public void getTwit(Twit t);
	
}
