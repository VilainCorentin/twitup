package com.iup.tp.twitup.listener;

import com.iup.tp.twitup.datamodel.User;

public interface IUsersListener {

	public void getOtherUser(User t);
	
	public void resetUsers();
	
}
