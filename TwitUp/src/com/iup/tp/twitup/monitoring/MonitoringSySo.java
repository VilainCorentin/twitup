package com.iup.tp.twitup.monitoring;

import com.iup.tp.twitup.datamodel.IDatabaseObserver;
import com.iup.tp.twitup.datamodel.Twit;
import com.iup.tp.twitup.datamodel.User;

public class MonitoringSySo implements IDatabaseObserver{

	@Override
	public void notifyTwitAdded(Twit addedTwit) {
		System.out.println("----- Twit ajouté -----");
	}

	@Override
	public void notifyTwitDeleted(Twit deletedTwit) {
		System.out.println("----- Twit supprimé -----");
	}

	@Override
	public void notifyTwitModified(Twit modifiedTwit) {
		System.out.println("----- Twit modifié -----");
	}

	@Override
	public void notifyUserAdded(User addedUser) {
		System.out.println("----- Utilisateur ajouté -----");
	}

	@Override
	public void notifyUserDeleted(User deletedUser) {
		System.out.println("----- Utilisateur supprimé -----");
	}

	@Override
	public void notifyUserModified(User modifiedUser) {
		System.out.println("----- Utilisateur modifié -----");
	}

}
