package com.iup.tp.twitup.view;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Insets;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import com.iup.tp.twitup.configue.Configurator;
import com.iup.tp.twitup.controller.ILoginUser;

public class LoginView extends JFrame implements ActionListener{

	private static final long serialVersionUID = -773228698713668388L;

	private JLabel labelUsername;
    private JLabel labelPassword;
    private JTextField textUsername;
    private JPasswordField fieldPassword;
    private JButton buttonLogin;
    private JLabel labelWarning;
    
    private Configurator config;
    
    public ILoginUser gestionData;
	
    public LoginView(ILoginUser d) {
		
    	super();
    	
    	this.config = new Configurator();
    	
    	this.gestionData = d;
    	
    	this.setTitle(config.getTranslate("TITLE_LOGIN"));
    	
    	labelUsername = new JLabel(config.getTranslate("LBL_LOGIN"));
        labelPassword = new JLabel(config.getTranslate("LBL_PASSW"));
        textUsername = new JTextField(20);
        fieldPassword = new JPasswordField(20);
        buttonLogin = new JButton(config.getTranslate("BTN_LOGIN_OK"));
        labelWarning = new JLabel();
        labelWarning.setVisible(false);
        
        // create a new panel with GridBagLayout manager
        JPanel newPanel = new JPanel(new GridBagLayout());
         
        newPanel.add(labelUsername, new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(10, 10, 10, 10), 0, 0));

        newPanel.add(textUsername, new GridBagConstraints(1, 0, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(10, 10, 10, 10), 0, 0));
             
        newPanel.add(labelPassword, new GridBagConstraints(0, 1, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(10, 10, 10, 10), 0, 0));
         
        newPanel.add(fieldPassword, new GridBagConstraints(1, 1, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(10, 10, 10, 10), 0, 0));
         
        newPanel.add(buttonLogin, new GridBagConstraints(0, 2, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(10, 10, 10, 10), 0, 0));
        
        newPanel.add(labelWarning, new GridBagConstraints(0, 3, 2, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(10, 10, 10, 10), 0, 0));
         
        // set border for the panel
        newPanel.setBorder(BorderFactory.createTitledBorder(
                BorderFactory.createEtchedBorder(), config.getTranslate("PANEL_LOGIN")));
         
        // add the panel to this frame
        add(newPanel);
        
        buttonLogin.addActionListener((event) -> {
    		Boolean isValid = this.gestionData.verifUser(textUsername.getText(), String.valueOf(fieldPassword.getPassword()));
    		
    		if(isValid) {
    			this.setVisible(false);
    			textUsername.setText("");
    			fieldPassword.setText("");
    			this.dispose();
    		}else {
    			this.labelWarning.setText(config.getTranslate("UNABLE"));
    			this.labelWarning.setVisible(true);
    			pack();
    		}
        });
        
        pack();
        setLocationRelativeTo(null);
    	
	}
    
	@Override
	public void actionPerformed(ActionEvent arg0) {
		
		this.setVisible(true);
		
	}
	
}
