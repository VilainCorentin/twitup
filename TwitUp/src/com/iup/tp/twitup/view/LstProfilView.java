package com.iup.tp.twitup.view;

import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import com.iup.tp.twitup.datamodel.User;
import com.iup.tp.twitup.listener.IProfilsListener;
import com.iup.tp.twitup.listener.IUserIsConnectedListener;
import com.iup.tp.twitup.listener.IUsersListener;
import com.iup.tp.twitup.model.UserConnected;
import com.iup.tp.twitup.model.UsersModel;

public class LstProfilView extends JScrollPane implements IUserIsConnectedListener, IUsersListener, IProfilsListener{

	private static final long serialVersionUID = 2391876025066722089L;
	
	protected JPanel panelScroll;
	
	protected UsersModel usersModel;
	
	protected UserConnected userModel;
	
	protected User userCo;
	
	public LstProfilView(UsersModel userModel, UserConnected userCo) {
		
		super(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		
		this.usersModel = userModel;
		
		this.userModel = userCo;
		
		this.userCo = null;
		
		this.panelScroll = new JPanel();
		
		this.panelScroll.setLayout((new BoxLayout(this.panelScroll, BoxLayout.Y_AXIS)));
		
		this.setViewportView(this.panelScroll);
		
		this.setVisible(false);
		
	}

	@Override
	public void getOtherUser(User u) {
		
		Boolean isSub = false;
		
		if(this.userCo != null) {
			isSub = this.userCo.isFollowing(u);
			if(u.getUserTag().equals(this.userCo.getUserTag())) {
				return;
			}
		}
		
		ProfilElemLst userOtherView = new ProfilElemLst(u, isSub);
		
		userOtherView.addUserListener(this);
		
		this.panelScroll.add(userOtherView);
		
		this.revalidate();
	}

	@Override
	public void userIsLog(Boolean bool) {
		this.setVisible(bool);
	}

	@Override
	public void getUser(User user) {
		this.userCo = user;
		this.resetUsers();
		this.usersModel.refreshModel();
	}

	@Override
	public void resetUsers() {
		
		this.panelScroll.removeAll();
		
	}

	@Override
	public void getNewSub(User u) {
		
		if(!this.userCo.isFollowing(u)) {
			this.userModel.addFollow(u);
		}else {
			this.userModel.removeFollow(u);
		}
		
	}
	
}
