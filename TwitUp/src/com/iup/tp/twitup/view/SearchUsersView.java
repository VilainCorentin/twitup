package com.iup.tp.twitup.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.iup.tp.twitup.configue.Configurator;
import com.iup.tp.twitup.controller.ISearchUser;
import com.iup.tp.twitup.datamodel.User;
import com.iup.tp.twitup.listener.IUserIsConnectedListener;

public class SearchUsersView extends JPanel implements IUserIsConnectedListener, ActionListener{

	private static final long serialVersionUID = 1746168946032401371L;

	private JTextField searchField;
	private JButton searchBtn;
	
	private ISearchUser controllerSearch;
	
	public SearchUsersView(ISearchUser controllerSearch) {
		super();
		
		this.controllerSearch = controllerSearch;
		
		searchField = new JTextField(20);
		searchBtn = new JButton(Configurator.getInstance().getTranslate("SEARCH_USER"));
		
		this.searchBtn.addActionListener(this);
		
		this.add(searchField);
		this.add(searchBtn);
		
		this.setVisible(false);
	}

	@Override
	public void userIsLog(Boolean bool) {
		
		this.setVisible(bool);
		
	}

	@Override
	public void getUser(User user) {}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		if(this.searchField.getText().length() > 0) {
			this.controllerSearch.searchUser(this.searchField.getText());
		}else {
			this.controllerSearch.cancelSearch();
		}
		
	}
	
}
