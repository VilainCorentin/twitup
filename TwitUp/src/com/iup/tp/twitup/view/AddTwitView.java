package com.iup.tp.twitup.view;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.iup.tp.twitup.configue.Configurator;
import com.iup.tp.twitup.controller.ICreateTwit;
import com.iup.tp.twitup.datamodel.User;
import com.iup.tp.twitup.listener.IUserIsConnectedListener;

public class AddTwitView extends JPanel implements IUserIsConnectedListener, ActionListener{

	private static final long serialVersionUID = 1828178012439532957L;

	private JLabel labelText;
    private JTextField textText;
    private JButton buttonSubmit;
    
    private ICreateTwit controller;
    
    public AddTwitView(ICreateTwit controller) {
		
    	super(new GridBagLayout());
    	
    	this.controller = controller;
    	
    	labelText = new JLabel(Configurator.getInstance().getTranslate("YOUR_TWIT"));
    	textText = new JTextField(150);
    	buttonSubmit = new JButton(Configurator.getInstance().getTranslate("TWITT"));
    	
        this.add(labelText, new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0,5,0,0), 0, 0));

        this.add(textText, new GridBagConstraints(0, 1, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0,0,0,0), 0, 0));
        
        this.add(buttonSubmit, new GridBagConstraints(0, 2, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0,0,0,0), 0, 0));
    
        this.buttonSubmit.addActionListener(this);
        
        this.setVisible(false);
        
	}

	@Override
	public void userIsLog(Boolean bool) {
		this.setVisible(bool);
	}

	@Override
	public void getUser(User user) {}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		if(this.textText.getText().length() > 150) {
			this.textText.setText(Configurator.getInstance().getTranslate("WARN_LONGER"));
		}else {
			this.controller.createTwit(this.textText.getText());
		}
	}
	
}
