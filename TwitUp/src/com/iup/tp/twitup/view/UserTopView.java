package com.iup.tp.twitup.view;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;

import com.iup.tp.twitup.configue.Configurator;
import com.iup.tp.twitup.controller.ILogoutUser;
import com.iup.tp.twitup.datamodel.User;
import com.iup.tp.twitup.debugging.ProgramTrace;
import com.iup.tp.twitup.listener.IUserIsConnectedListener;
import com.iup.tp.twitup.model.UserConnected;

public class UserTopView extends JPanel implements IUserIsConnectedListener, ActionListener{

	private static final long serialVersionUID = 7395818161747477885L;

	private JLabel labelUsername;
	private JLabel labelTag;
	private JButton logoutBtn;
	private JButton profilBtn;
	
	private ProfilView profilView;
	
	private ILogoutUser logoutController;
	
	public UserTopView(ILogoutUser logoutC, UserConnected userCo) {
		super(new GridBagLayout());
		
		this.logoutController = logoutC;
		
		labelUsername = new JLabel("");
		labelTag = new JLabel("");
		profilBtn = new JButton("Profil");
		profilBtn.setVisible(false);
		logoutBtn = new JButton(Configurator.getInstance().getTranslate("DECO"));
		logoutBtn.setVisible(false);
		
		this.logoutBtn.addActionListener(this);
		
		this.profilView = new ProfilView(userCo);
		this.profilBtn.addActionListener(this.profilView);
		
		this.add(labelUsername, new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 5, 0, 0), 0, 0));
		this.add(labelTag, new GridBagConstraints(1, 0, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
		this.add(profilBtn, new GridBagConstraints(2, 0, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
		this.add(logoutBtn, new GridBagConstraints(3, 0, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
		
		this.add(new JSeparator(), new GridBagConstraints(0, 1, 3, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
		
	}

	@Override
	public void userIsLog(Boolean bool) {
		this.setVisible(bool);
	}

	@Override
	public void getUser(User user) {
		if(user != null) {
			logoutBtn.setVisible(true);
			profilBtn.setVisible(true);
			this.labelUsername.setText(Configurator.getInstance().getTranslate("WELCOME") + user.getName() + " !");
			this.labelTag.setText("tag : " + user.getUserTag());
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		
		ProgramTrace.getInstance().displayStackTrace();
		
		this.logoutController.logoutUser();
		
	}
	
}
