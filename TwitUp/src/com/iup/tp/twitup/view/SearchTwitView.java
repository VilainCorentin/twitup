package com.iup.tp.twitup.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.iup.tp.twitup.configue.Configurator;
import com.iup.tp.twitup.controller.ISearchTwit;
import com.iup.tp.twitup.datamodel.User;
import com.iup.tp.twitup.listener.IUserIsConnectedListener;

public class SearchTwitView extends JPanel implements IUserIsConnectedListener, ActionListener{

	private static final long serialVersionUID = -8432624082920711838L;

	private JTextField searchField;
	private JButton searchBtn;
	
	private ISearchTwit controllerSearch;
	
	public SearchTwitView(ISearchTwit controllerSearch) {
		
		this.controllerSearch = controllerSearch;
		
		searchField = new JTextField(20);
		searchBtn = new JButton(Configurator.getInstance().getTranslate("SEARCH_TWIT"));
		
		this.searchBtn.addActionListener(this);
		
		this.add(searchField);
		this.add(searchBtn);
		
		this.setVisible(false);
		
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {

		if(this.searchField.getText().length() > 0) {
			this.controllerSearch.searchTwit(this.searchField.getText());
		}else {
			this.controllerSearch.cancelSearchTwit();
		}
		
	}

	@Override
	public void userIsLog(Boolean bool) {
		this.setVisible(bool);
	}

	@Override
	public void getUser(User user) {}

	
	
}
