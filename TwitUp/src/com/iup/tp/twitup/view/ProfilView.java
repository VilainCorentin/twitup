package com.iup.tp.twitup.view;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import com.iup.tp.twitup.model.UserConnected;

public class ProfilView extends JFrame implements ActionListener{

	private static final long serialVersionUID = -3322510906928009881L;

	private JLabel labelTag;
	
	private JLabel labelUsername;
	private JTextField fieldUsername;
	
	private JScrollPane scrollFollowers;
	
	private JLabel avatar;
	
	private UserConnected user;
	
	private JButton btnChangeName;
	private JButton btnChangeAvatar;
	
	JPanel panelScroll;
	
	public ProfilView(UserConnected user) {
		
		super();
		
		this.user = user;
		
		this.setTitle("Profil");
		
		this.setLayout(new GridBagLayout());
		
		avatar = new JLabel();
		
		labelTag = new JLabel();
		
		labelUsername = new JLabel();
		fieldUsername = new JTextField(20);
		
		btnChangeName = new JButton("Change Tag");
		btnChangeAvatar = new JButton("Change Avatar");
		
		scrollFollowers = new JScrollPane(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		
		panelScroll = new JPanel();
		panelScroll.setLayout((new BoxLayout(panelScroll, BoxLayout.Y_AXIS)));
		scrollFollowers.setViewportView(panelScroll);
		
		this.add(avatar, new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(10, 10, 10, 10), 0, 0));
		
		this.add(btnChangeAvatar, new GridBagConstraints(1, 0, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(10, 10, 10, 10), 0, 0));

		this.add(labelTag, new GridBagConstraints(0, 1, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(10, 10, 10, 10), 0, 0));
		
		this.add(labelUsername, new GridBagConstraints(0, 2, 1, 1, 1, 1, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(10, 10, 10, 10), 0, 0));
		
		this.add(fieldUsername, new GridBagConstraints(1, 2, 1, 1, 1, 1, GridBagConstraints.EAST, GridBagConstraints.BOTH, new Insets(10, 10, 10, 10), 0, 0));
		
		this.add(btnChangeName, new GridBagConstraints(0, 3, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(10, 10, 10, 10), 0, 0));
		
		this.add(scrollFollowers, new GridBagConstraints(0, 4, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(10, 10, 10, 10), 0, 0));
		
	    btnChangeAvatar.addActionListener(
				e->this.changeAvatar()
		);
	    
		btnChangeName.addActionListener(
				e->this.changeUserName(this.fieldUsername.getText())
		);
		
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		
		if(this.user.getUser().getAvatarPath() != null) {
			ImageIcon pImg = new ImageIcon(this.user.getUser().getAvatarPath());
			Image image = pImg.getImage();
			Image newimg = image.getScaledInstance(200, 200,  java.awt.Image.SCALE_SMOOTH);
			pImg = new ImageIcon(newimg);
			avatar.setIcon(pImg);
		}
		
		labelUsername.setText(" Mon pseudo : " + this.user.getUser().getName());
		labelTag.setText("Mon tag : " + this.user.getUser().getUserTag());
		
		this.panelScroll.removeAll();
		
		for(String follow : this.user.getUser().getFollows()) {
			this.panelScroll.add(new JLabel(follow));
		}
		
		this.setVisible(true);
		
		revalidate();
		
		pack();
		
	}
	
	private void changeUserName(String s) {
		this.user.changeUsername(s);
		labelUsername.setText(" Mon pseudo : " + this.user.getUser().getName());
	}
	
	private void changeAvatar() {
		JFileChooser fc = new JFileChooser();
	    if(fc.showOpenDialog(this) == JFileChooser.APPROVE_OPTION){
	        BufferedImage img = null;
			try {
				img = ImageIO.read(fc.getSelectedFile());
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			
			this.user.changeAvatar(fc.getSelectedFile().getAbsolutePath());
			
			ImageIcon pImg = new ImageIcon(img);
			Image image = pImg.getImage();
			Image newimg = image.getScaledInstance(200, 200,  java.awt.Image.SCALE_SMOOTH);
			pImg = new ImageIcon(newimg);
	        avatar.setIcon(pImg);
	    }
	}

}
