package com.iup.tp.twitup.view;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.BevelBorder;

import com.iup.tp.twitup.datamodel.Twit;

public class TwitView extends JPanel{

	private static final long serialVersionUID = -8381532792022378469L;

	private JLabel labelAuthor;
	private JLabel labelText;
	
	private Twit twit;
	
	public TwitView(Twit t) {
		super();
		
		this.twit = t;
		
		labelAuthor = new JLabel(this.twit.getText());
		labelText = new JLabel("par " + this.twit.getTwiter().getName());
		
		this.add(labelAuthor);
		this.add(labelText);
		
		this.setBorder(new javax.swing.border.BevelBorder(BevelBorder.RAISED));
		
		this.setVisible(true);
	}
	
}