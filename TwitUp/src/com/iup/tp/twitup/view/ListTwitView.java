package com.iup.tp.twitup.view;

import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import com.iup.tp.twitup.datamodel.Twit;
import com.iup.tp.twitup.datamodel.User;
import com.iup.tp.twitup.listener.ITwitListener;
import com.iup.tp.twitup.listener.IUserIsConnectedListener;

public class ListTwitView extends JScrollPane implements ITwitListener, IUserIsConnectedListener{

	private static final long serialVersionUID = 2391876025066722089L;
	
	protected JPanel panelScroll;
	
	public ListTwitView() {
		
		super(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		
		this.panelScroll = new JPanel();
		
		this.panelScroll.setLayout((new BoxLayout(this.panelScroll, BoxLayout.Y_AXIS)));
		
		this.setViewportView(this.panelScroll);
		
		this.setVisible(false);
		
	}
	
	@Override
	public void getTwit(Twit t) {
		
		TwitView twitView = new TwitView(t);
		
		this.panelScroll.add(twitView);
		
		this.revalidate();
	}
	
	public void resetTwit() {
		
		this.panelScroll.removeAll();
		
	}

	@Override
	public void userIsLog(Boolean bool) {
		this.setVisible(bool);
	}

	@Override
	public void getUser(User user) {

	}
	
}
