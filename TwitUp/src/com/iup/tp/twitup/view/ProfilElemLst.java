package com.iup.tp.twitup.view;

import java.util.ArrayList;
import java.util.Collection;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.BevelBorder;

import com.iup.tp.twitup.configue.Configurator;
import com.iup.tp.twitup.datamodel.User;
import com.iup.tp.twitup.listener.IProfilsListener;

public class ProfilElemLst extends JPanel{

	private static final long serialVersionUID = -7501042431018970742L;

	private JLabel labelName;
	private JLabel labelTag;
	
	private JButton subBtn;
	
	private Boolean isSub;
	
	private User user;
	
	private final Collection<IProfilsListener> elemListeners = new ArrayList<>();
	
	public ProfilElemLst(User u, Boolean sub) {
		this.user = u;
		
		this.isSub = sub;
		
		if(this.isSub) {
			this.subBtn = new JButton(Configurator.getInstance().getTranslate("DESUB"));
		}else {
			this.subBtn = new JButton(Configurator.getInstance().getTranslate("SUB"));
		}
		
		labelName = new JLabel("Username : " + this.user.getName());
		labelTag = new JLabel("Tag : " + this.user.getUserTag());
		
		this.add(labelName);
		this.add(labelTag);
		
		this.add(subBtn);
		
		this.setBorder(new javax.swing.border.BevelBorder(BevelBorder.RAISED));
		
		subBtn.addActionListener(e -> {
			for(IProfilsListener p : this.elemListeners) {
				p.getNewSub(this.user);
			}
		});
		
		this.setVisible(true);
	}
	
    public void addUserListener(IProfilsListener listener) {
        elemListeners.add(listener);
    }
	
}
