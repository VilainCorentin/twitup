package com.iup.tp.twitup.view;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashSet;
import java.util.UUID;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import com.iup.tp.twitup.configue.Configurator;
import com.iup.tp.twitup.controller.IUserRegister;
import com.iup.tp.twitup.datamodel.User;

public class RegisterView extends JFrame implements ActionListener{

	private JLabel labelUsername;
	private JLabel labelTag;
    private JLabel labelPassword;
    private JTextField textUsername;
    private JTextField textTag;
    private JPasswordField fieldPassword;
    private JButton buttonRegister;
    private JLabel labelWarning;
    
    private Configurator config;
    public IUserRegister gestionData;
	
	private static final long serialVersionUID = -8804646422890639322L;
	
	public RegisterView(IUserRegister d) {

		super();
		
		this.gestionData = d;
    	
    	this.config = new Configurator();
    	
    	this.setTitle(config.getTranslate("TITLE_REGISTER"));
    	
    	labelUsername = new JLabel(config.getTranslate("LBL_LOGIN"));
        labelPassword = new JLabel(config.getTranslate("LBL_PASSW"));
        labelTag = new JLabel(config.getTranslate("TAG_REGISTER"));
        textUsername = new JTextField(20);
        textTag = new JTextField(20);
        fieldPassword = new JPasswordField(20);
        buttonRegister = new JButton(config.getTranslate("BTN_REGISTER_OK"));
        labelWarning = new JLabel();
        labelWarning.setVisible(false);
        
        // create a new panel with GridBagLayout manager
        JPanel newPanel = new JPanel(new GridBagLayout());
         
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.anchor = GridBagConstraints.WEST;
        constraints.insets = new Insets(10, 10, 10, 10);
         
        // add components to the panel
        constraints.gridx = 0;
        constraints.gridy = 0;     
        newPanel.add(labelUsername, constraints);
 
        constraints.gridx = 1;
        newPanel.add(textUsername, constraints);
        
        constraints.gridx = 0;
        constraints.gridy = 1;     
        newPanel.add(labelTag, constraints);
 
        constraints.gridx = 1;
        newPanel.add(textTag, constraints);
         
        constraints.gridx = 0;
        constraints.gridy = 2;     
        newPanel.add(labelPassword, constraints);
         
        constraints.gridx = 1;
        newPanel.add(fieldPassword, constraints);
         
        constraints.gridx = 0;
        constraints.gridy = 3;
        constraints.gridwidth = 2;
        constraints.anchor = GridBagConstraints.CENTER;
        newPanel.add(buttonRegister, constraints);
        
        constraints.gridx = 0;
        constraints.gridy = 4;
        constraints.gridwidth = 2;
        constraints.anchor = GridBagConstraints.CENTER;
        newPanel.add(labelWarning, constraints);
         
        // set border for the panel
        newPanel.setBorder(BorderFactory.createTitledBorder(
                BorderFactory.createEtchedBorder(), config.getTranslate("PANEL_REGISTER")));
         
        // add the panel to this frame
        add(newPanel);
        
        buttonRegister.addActionListener((event) -> {
        	
        	if(textTag.getText().isEmpty() || fieldPassword.getPassword().length == 0) {
        		this.labelWarning.setText("Les champs doivent être remplis");
        		this.labelWarning.setVisible(true);
                pack();
        		return;
        	}
        	
        	User user = new User(UUID.randomUUID(), textTag.getText(), String.valueOf(fieldPassword.getPassword()), textUsername.getText(), new HashSet<String>(), "");
    		Boolean isValid = this.gestionData.addUserFromRegister(user);	
    		
    		if(!isValid) {
    			this.labelWarning.setText("User's Tag already use : " + user.getUserTag());
    		}else {
    			this.labelWarning.setText("Register completed");
    		}
    		
    		this.labelWarning.setVisible(true);
    		pack();
        });
         
        setLocationRelativeTo(null);
		
        pack();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		this.setVisible(true);
		
	}

}
