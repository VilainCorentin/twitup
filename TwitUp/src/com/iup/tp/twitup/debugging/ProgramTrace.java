package com.iup.tp.twitup.debugging;

import java.util.ArrayList;

public class ProgramTrace {

	private static final ProgramTrace instance = new ProgramTrace(-1);

	private ArrayList<Trace> lstTrace;
	
	private int filterLevel;
	
	private int ind;
	
	public ProgramTrace(int filterLevel) {
		
		this.lstTrace = new ArrayList<>();
		
		this.ind = 0;
		
		this.filterLevel = filterLevel;
		
	}
	
	public void addTrace(Trace t) {
		
		if(t.level < this.filterLevel) {
			return;
		}
		
		if(this.ind > 0) {
			Trace prev = this.lstTrace.get(this.ind -1);
			t.setPrec(prev);
			prev.setNext(t);
			this.ind++;
		}
		
		this.lstTrace.add(t);
		this.ind++;
		
	}
	
	public ArrayList<Trace> getTraces(){
		return this.lstTrace;
	}
	
	public void displayStackTrace() {
		
		for(Trace t : this.lstTrace) {
			
			System.out.println("-------");
			
			System.out.println("DATE : " + t.dateFormat.format(t.date));
			System.out.println("LEVEL : " + t.level);
			System.out.println("METHOD : " + t.method);
			System.out.println("NAME VAL : " + t.nameValue);
			System.out.println("VALUE VAL : " + t.valueVal);
			
		}
	}
	
	public void setLevel(int level) {
		this.filterLevel = level;
	}
	
	public static final ProgramTrace getInstance() 
    {
        return instance;
    }
	
}
