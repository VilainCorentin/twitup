package com.iup.tp.twitup.debugging;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Trace {
	
	protected DateFormat dateFormat;
	protected Date date;
	
	protected int level;
	
	protected String method;
	
	protected String nameValue;
	protected Object valueVal;
	
	protected Trace next;
	protected Trace prec;
	
	public Trace(int lvl, String method, String nameV, Object valueV) {
		this.dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		this.date = new Date();
		
		this.level = lvl;
		
		this.method = method;
		
		this.nameValue = nameV;
		this.valueVal = valueV;
		
		this.next = null;
		this.prec = null;
	}

	public Trace getNext() {
		return next;
	}

	public void setNext(Trace next) {
		this.next = next;
	}

	public Trace getPrec() {
		return prec;
	}

	public void setPrec(Trace prec) {
		this.prec = prec;
	}
	
}
