package com.iup.tp.twitup.controller;

import java.util.HashSet;
import java.util.Set;

import com.iup.tp.twitup.datamodel.IDatabase;
import com.iup.tp.twitup.datamodel.User;
import com.iup.tp.twitup.model.UsersModel;
import com.iup.tp.twitup.view.LstProfilView;

public class SearchUserController implements ISearchUser{

	protected UsersModel usersM;
	protected IDatabase data;
	protected LstProfilView lstProfils;
	
	public SearchUserController(UsersModel usersM, IDatabase data, LstProfilView lstProfil) {
		super();
		this.usersM = usersM;
		this.data = data;
		this.lstProfils = lstProfil;
	}

	@Override
	public void searchUser(String s) {
	
		Set<User> users = new HashSet<>();
		
		for(User u : this.data.getUsers()) {
			if(u.getUserTag().indexOf(s) != -1) {
				users.add(u);
			}
		}
		
		this.lstProfils.resetUsers();
		
		this.usersM.setModel(users);
		
	}

	@Override
	public void cancelSearch() {

		this.lstProfils.resetUsers();
		
		this.usersM.setModel(this.data.getUsers());
		
	}

}
