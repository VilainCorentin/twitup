package com.iup.tp.twitup.controller;

import com.iup.tp.twitup.core.EntityManager;
import com.iup.tp.twitup.datamodel.Twit;
import com.iup.tp.twitup.model.UserConnected;

public class AddTwitController implements ICreateTwit{

	protected EntityManager manager;
	protected UserConnected actualUser;
	
	public AddTwitController(EntityManager manager, UserConnected actualUser) {
		super();
		this.manager = manager;
		this.actualUser = actualUser;
	}

	@Override
	public void createTwit(String text) {
		this.manager.sendTwit(new Twit(this.actualUser.getUser(), text));
	}

}
