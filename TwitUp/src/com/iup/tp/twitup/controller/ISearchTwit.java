package com.iup.tp.twitup.controller;

public interface ISearchTwit {

	public void searchTwit(String s);
	
	public void cancelSearchTwit();
	
}
