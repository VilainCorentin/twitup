package com.iup.tp.twitup.controller;

import com.iup.tp.twitup.model.UserConnected;

public class LogoutController implements ILogoutUser{

	protected UserConnected userConnected;

	public LogoutController(UserConnected userConnected) {
		super();
		this.userConnected = userConnected;
	}

	@Override
	public void logoutUser() {

		this.userConnected.setUser(null);
		
	}
	
}
