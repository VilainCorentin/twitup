package com.iup.tp.twitup.controller;

import java.util.HashSet;
import java.util.Set;

import com.iup.tp.twitup.datamodel.IDatabase;
import com.iup.tp.twitup.datamodel.Twit;
import com.iup.tp.twitup.model.TwitModel;
import com.iup.tp.twitup.view.ListTwitView;

public class SearchTwitController implements ISearchTwit{

	protected TwitModel twitM;
	protected IDatabase data;
	protected ListTwitView lstTwits;
	
	public SearchTwitController(TwitModel usersM, IDatabase data, ListTwitView lstTwits) {
		super();
		this.twitM = usersM;
		this.data = data;
		this.lstTwits = lstTwits;
	}

	@Override
	public void searchTwit(String s) {
		
		Set<Twit> twits = new HashSet<>();
		
		Boolean byUser = s.indexOf("@") !=-1? true: false;
		Boolean byTag = s.indexOf("#") !=-1? true: false;
		
		s = s.replace("@", "");
		
		if(byUser) {
			
			for(Twit t : this.data.getTwits()) {
				if(t.getTwiter().getUserTag().equals(s)) {
					twits.add(t);
				}
			}
			
		}else if(byTag){
			
			for(Twit t : this.data.getTwits()) {
				if(t.getText().indexOf(s) != -1) {
					twits.add(t);
				}
			}
			
		}else {
			
			for(Twit t : this.data.getTwits()) {
				if(t.getTwiter().getUserTag().equals(s)) {
					twits.add(t);
				}
			}
			
			for(Twit t : this.data.getTwits()) {
				if(t.getText().indexOf(s) != -1) {
					twits.add(t);
				}
			}
			
		}

		this.lstTwits.resetTwit();
		
		this.twitM.setModel(twits);
		
	}

	@Override
	public void cancelSearchTwit() {
		
		this.lstTwits.resetTwit();
		
		this.twitM.setModel(this.data.getTwits());
		
	}

}
