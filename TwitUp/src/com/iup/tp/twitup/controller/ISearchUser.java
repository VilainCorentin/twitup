package com.iup.tp.twitup.controller;

public interface ISearchUser {

	public void searchUser(String s);
	
	public void cancelSearch();
	
}
