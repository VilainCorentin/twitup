package com.iup.tp.twitup.controller;

import com.iup.tp.twitup.core.EntityManager;
import com.iup.tp.twitup.datamodel.IDatabase;
import com.iup.tp.twitup.datamodel.User;

public class RegisterController implements IUserRegister{

	public EntityManager mEntityManager;
	public IDatabase dataBase;
	
	public RegisterController(EntityManager mEntityManager, IDatabase dataBase) {
		super();
		this.dataBase = dataBase;
		this.mEntityManager = mEntityManager;
	}

	@Override
	public Boolean addUserFromRegister(User u) {
		
		Boolean isValid = true;
		
		for(User user : this.dataBase.getUsers()) {
			if(user.getUserTag().equals(u.getUserTag())) {
				isValid = false;
			}
		}
		
		if(isValid) {
			this.mEntityManager.sendUser(u);
		}
		
		return isValid;
	}

}
