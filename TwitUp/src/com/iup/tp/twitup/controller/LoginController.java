package com.iup.tp.twitup.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

import com.iup.tp.twitup.datamodel.IDatabase;
import com.iup.tp.twitup.datamodel.User;
import com.iup.tp.twitup.listener.IUserListener;

public class LoginController implements ILoginUser{

	private final Collection<IUserListener> userListeners = new ArrayList<IUserListener>();
	
	protected IDatabase mDatabase;
	protected User user = null;

	public LoginController(IDatabase mDatabase) {
		super();
		this.mDatabase = mDatabase;
	}

    public void addUserListener(IUserListener listener) {
        userListeners.add(listener);
    }
	
	@Override
	public Boolean verifUser(String tag, String pass) {
		
		HashSet<User> allUsersSet = new HashSet<User>(this.mDatabase.getUsers());
		
		Boolean isValid = false;
		
		for(User u : allUsersSet) {
			
			if(u.getUserTag().equals(tag) && u.getUserPassword().toString().equals(pass)) {
				this.user = u;
				isValid = true;
				
				for(IUserListener listener : this.userListeners) {
					listener.logUser(this.user);
				}
				
			}
		}
		
		return isValid;
		
	}
	
}
