package com.iup.tp.twitup.controller;

import com.iup.tp.twitup.datamodel.User;

public interface IUserRegister {

	public Boolean addUserFromRegister(User u);
	
}
