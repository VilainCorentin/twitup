package com.iup.tp.twitup.controller;

import com.iup.tp.twitup.configue.Configurator;
import com.iup.tp.twitup.core.EntityManager;
import com.iup.tp.twitup.datamodel.IDatabase;
import com.iup.tp.twitup.datamodel.IDatabaseObserver;
import com.iup.tp.twitup.datamodel.Twit;
import com.iup.tp.twitup.datamodel.User;
import com.iup.tp.twitup.debugging.ProgramTrace;
import com.iup.tp.twitup.debugging.Trace;
import com.iup.tp.twitup.ihm.LoginRegisterView;
import com.iup.tp.twitup.ihm.TwitupMainView;
import com.iup.tp.twitup.listener.IUserListener;
import com.iup.tp.twitup.model.TwitModel;
import com.iup.tp.twitup.model.UserConnected;
import com.iup.tp.twitup.model.UsersModel;
import com.iup.tp.twitup.view.AddTwitView;
import com.iup.tp.twitup.view.ListTwitView;
import com.iup.tp.twitup.view.LstProfilView;
import com.iup.tp.twitup.view.SearchTwitView;
import com.iup.tp.twitup.view.SearchUsersView;
import com.iup.tp.twitup.view.UserTopView;

public class MainController implements IUserListener, IDatabaseObserver{
	
	private LoginRegisterView loginRegisterView;
	
	protected TwitupMainView mainView;
	
	protected UserTopView userView;
	
	protected AddTwitView addTwitView;
	
	protected ListTwitView lstTwitView;
	
	protected LstProfilView lstProfilUsersView;
	
	protected SearchUsersView searchUserView;
	
	protected SearchTwitView searchTwitView;
	
	protected IDatabase mDatabase;

	protected EntityManager mEntityManager;
	
	protected LoginController iLoginUser;
	
	protected RegisterController iRegister;
	
	protected AddTwitController addTwitController;
	
	protected SearchUserController searchUserController;
	
	protected SearchTwitController searchTwitController;
	
	protected UserConnected user;
	
	protected TwitModel twitModel;
	
	private Configurator translate;
	
	protected UsersModel usersModel;
	
	protected LogoutController logoutController;
	
	public MainController(IDatabase dataBase, EntityManager manager) {
		
		this.mainView = new TwitupMainView();
		
		this.translate = new Configurator();
		
		this.mDatabase = dataBase;
		
		this.twitModel = new TwitModel(this.mDatabase.getTwits());
		
		this.usersModel = new UsersModel(this.mDatabase.getUsers());
		
		this.user = new UserConnected(manager);
		
		this.mEntityManager = manager;
		
		this.addTwitController = new AddTwitController(mEntityManager, user);
		
		this.addTwitView = new AddTwitView(addTwitController);
		
		this.iLoginUser = new LoginController(this.mDatabase);
		this.iLoginUser.addUserListener(this);
		
		this.iRegister = new RegisterController(this.mEntityManager, this.mDatabase);
		
		this.loginRegisterView = new LoginRegisterView(iRegister, iLoginUser);
		this.user.addUserListener(this.loginRegisterView);
		
		this.logoutController = new LogoutController(this.user);
		
		this.userView = new UserTopView(this.logoutController, this.user);
		this.user.addUserListener(this.userView);
		this.user.addUserListener(this.addTwitView);
		
		this.mDatabase.addObserver(this);
		
		this.lstProfilUsersView = new LstProfilView(this.usersModel, this.user);
		this.usersModel.addUserListener(lstProfilUsersView);
		this.user.addUserListener(lstProfilUsersView);
		
		this.lstTwitView = new ListTwitView();
		this.twitModel.addUserListener(this.lstTwitView);
		this.user.addUserListener(this.lstTwitView);
		
		this.searchUserController = new SearchUserController(this.usersModel, this.mDatabase, this.lstProfilUsersView);
		
		this.searchTwitController = new SearchTwitController(this.twitModel, this.mDatabase, this.lstTwitView);
		
		this.searchUserView = new SearchUsersView(this.searchUserController);
		this.user.addUserListener(this.searchUserView);
		
		this.searchTwitView = new SearchTwitView(this.searchTwitController);
		this.user.addUserListener(this.searchTwitView);
		
		this.mainView.setLoginRegisterPanel(this.loginRegisterView);
		this.mainView.setUserPanel(this.userView);
		this.mainView.setTwitsPanel(this.lstTwitView);
		this.mainView.setAddTwitPanel(this.addTwitView);
		this.mainView.setUsersPanel(lstProfilUsersView);
		this.mainView.setUserSearch(this.searchUserView);
		this.mainView.setTwitSearch(this.searchTwitView);
		
		this.mainView.setVisible(true);
		
	}

	@Override
	public void logUser(User user) {
		
		ProgramTrace.getInstance().addTrace(new Trace(0, Thread.currentThread().getStackTrace()[1].getMethodName(), "user", user.toString()));
		
		this.user.setUser(user);
	}

	@Override
	public void notifyTwitAdded(Twit addedTwit) {
		
		if(this.user.getUser() != null) {
			if(this.user.getUser().isFollowing(addedTwit.getTwiter())) {
				this.mainView.popupNotif(this.translate.getTranslate("NEW_TWIT") + addedTwit.getTwiter().getUserTag());
			}
		}
		
		this.twitModel.addTwit(addedTwit);
	}

	@Override
	public void notifyTwitDeleted(Twit deletedTwit) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void notifyTwitModified(Twit modifiedTwit) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void notifyUserAdded(User addedUser) {
		this.usersModel.addUserOther(addedUser);
		
	}

	@Override
	public void notifyUserDeleted(User deletedUser) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void notifyUserModified(User modifiedUser) {
		// TODO Auto-generated method stub
		
	}
	
}