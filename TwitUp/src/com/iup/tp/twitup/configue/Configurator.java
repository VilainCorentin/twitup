package com.iup.tp.twitup.configue;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Properties;

public class Configurator {
	
	private static final Configurator instance = new Configurator();
	
	private Properties props;
	private Properties propsLangage;

	public Configurator() {
		
		this.props = new Properties();
		this.propsLangage = new Properties();
		
		try {
			this.props.load(this.loadInputStreamProp("configuration.properties"));
			this.propsLangage.load(this.loadInputStreamProp(this.getProp("LANGAGE") + ".properties"));
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	
	}
	
	public void setProp(String key, String val) {
		this.props.setProperty(key, val);
		this.writeProperties();
	}
	
	private FileInputStream loadInputStreamProp(String s) {
		
		URL resource = getClass().getClassLoader().getResource(s);
		
		File file = null;
		
		InputStream input = null;
		
		try {
			file = new File(resource.toURI());
			input = new FileInputStream(file);
		} catch (URISyntaxException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		return (FileInputStream) input;
		
	}
	
	private void writeProperties() {
		
		URL resource = getClass().getClassLoader().getResource("configuration.properties");
		File file = null;
		try {
			file = new File(resource.toURI());
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		
        try {
			OutputStream out = new FileOutputStream(file);
			this.props.store(out, "save preferences");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
        
	}
	
	public String getProp(String s) {
		return this.props.getProperty(s);
	}
	
	public String getTranslate(String s) {
		return this.propsLangage.getProperty(s);
	}
	
	public static final Configurator getInstance() 
    {
        return instance;
    }
}
