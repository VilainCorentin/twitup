import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import com.iup.tp.twitup.debugging.ProgramTrace;
import com.iup.tp.twitup.debugging.Trace;

class TestTrace {

	@Test
	
	void testLevel() {
		
		ProgramTrace.getInstance().setLevel(2);
		
		ProgramTrace.getInstance().addTrace(new Trace(0, "test", "test", "test"));
		ProgramTrace.getInstance().addTrace(new Trace(1, "test", "test", "test"));
		ProgramTrace.getInstance().addTrace(new Trace(2, "test", "test", "test"));
		ProgramTrace.getInstance().addTrace(new Trace(3, "test", "test", "test"));
		
		assertEquals(ProgramTrace.getInstance().getTraces().size(), 2);
		
	}
	
	@Test
	void testPrec() {
		
		
		
	}
	
	@Test
	void testNext() {
		
		
		
	}
	
	@Test
	void testMethode() {
		
		
		
	}
	
	@Test
	void testDate() {
		
		
		
	}
	
	@Test
	void testVal() {
		
		
		
	}

}
